from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Status, Comment
from profileLingin.models import Member


# Create your tests here.
class StatusTest(TestCase):
    def test_status_url_exists(self):
        Member.objects.create(name='joko', birthday='01 Jan', gender='Male',
                              expertise='Marketing, Collector, Public Speaking',
                              description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com',
                              id="1")
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_root_redirect_to_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)

    def test_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

    def test_model_can_create(self):
        Status.objects.create(status="I'm currently happy")

        counter_all_available_activity = Status.objects.all().count()
        self.assertEqual(counter_all_available_activity, 1)

    def test_add_status_can_POST(self):
        Member.objects.create(name='joko', birthday='01 Jan', gender='Male',
                              expertise='Marketing, Collector, Public Speaking',
                              description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com',
                              id="1")
        response = self.client.post('/status/add/', data={'status': "I'm currently happy"})
        counter_all_available_activity = Status.objects.all().count()
        self.assertEqual(counter_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/status/')
        new_response = self.client.get('/status/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("currently happy", html_response)

    def test_add_status_reject_invalid_POST(self):
        Member.objects.create(name='joko', birthday='01 Jan', gender='Male',
                              expertise='Marketing, Collector, Public Speaking',
                              description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com',
                              id="1")
        response = self.client.post('/status/add/', data={'status': ''})
        html_response = response.content.decode('utf8')
        self.assertIn("alert alert-danger", html_response)

    def test_add_status_GET_redirects(self):
        Member.objects.create(name='joko', birthday='01 Jan', gender='Male',
                              expertise='Marketing, Collector, Public Speaking',
                              description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com',
                              id="1")
        response = self.client.get('/status/add/')
        self.assertEqual(response.status_code, 302)

    def test_add_comment_can_POST(self):
        Member.objects.create(name='joko', birthday='01 Jan', gender='Male',
                              expertise='Marketing, Collector, Public Speaking',
                              description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com',
                              id="1")
        Status.objects.create(status="I'm currently happy")
        response = self.client.post('/status/comment/1/', data={'name': "Sat", 'text': "This is a test comment"})

        counter_all_available_activity = Comment.objects.all().count()
        self.assertEqual(counter_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/status/')
        new_response = self.client.get('/status/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("This is a test comment", html_response)

    def test_add_comment_reject_invalid_POST(self):
        Member.objects.create(name='joko', birthday='01 Jan', gender='Male',
                              expertise='Marketing, Collector, Public Speaking',
                              description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com',
                              id="1")
        Status.objects.create(status="I'm currently happy")
        response = self.client.post('/status/comment/1/', data={})
        html_response = response.content.decode('utf8')
        self.assertIn("alert alert-danger", html_response)

    def test_comment_GET_exists(self):
        Status.objects.create(status="I'm currently happy")
        response = self.client.get('/status/comment/1/')
        self.assertEqual(response.status_code, 200)

    def test_delete_status_WORKS(self):
        Status.objects.create(status="I'm currently happy")
        self.client.post('/status/delete/status/1/')
        counter_all_available_activity = Status.objects.all().count()
        self.assertEqual(counter_all_available_activity, 0)

    def test_delete_comment_WORK(self):
        status = Status.objects.create(status="I'm currently happy")
        comment = Comment.objects.create(name='Zafat',text='Heyy whats up',status=status)
        response = self.client.post('/status/delete/comment/' + str(comment.id) +'/')
        counter_all_available_activity = Comment.objects.all().count()
        self.assertEqual(counter_all_available_activity, 0)