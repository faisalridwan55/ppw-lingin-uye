from django.db import models
from profileLingin.models import Member


class Status(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    status = models.TextField(max_length=140)


class Comment(models.Model):
    name = models.CharField(max_length=140, default="Anonymous")
    text = models.CharField(max_length=140)
    status = models.ForeignKey(Status)
    created_on = models.DateTimeField(auto_now_add=True)
