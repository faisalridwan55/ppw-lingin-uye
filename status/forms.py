from django import forms


class StatusForm(forms.Form):
    status = forms.CharField(max_length=140)


class CommentForm(forms.Form):
    name = forms.CharField(max_length=140)
    text = forms.CharField(max_length=140)