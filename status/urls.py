from django.conf.urls import url
from .views import index, add, comment, delete_comment, delete_status

# url for app

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add/$', add, name='add'),
    url(r'^comment/(?P<id>\d)/$', comment, name='comment'),
    url(r'^delete/comment/(?P<id>\d)/$', delete_comment, name='delete_comment'),
    url(r'^delete/status/(?P<id>\d)/$', delete_status, name='delete_status'),
]