from django.contrib import admin
from profileLingin.models import Member


class MyAdminSite(admin.AdminSite):
    pass

admin_site = MyAdminSite()
admin_site.register(Member)