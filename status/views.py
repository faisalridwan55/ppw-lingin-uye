from django.shortcuts import render, redirect
from .models import Status, Comment
from .forms import StatusForm, CommentForm
from profileLingin.models import Member


# Create your views here.
def index(request):
    response = {'name': Member.objects.get(id=1).name}
    status = Status.objects.all()
    response['commentform'] = CommentForm
    response['status'] = status
    return render(request, 'index.html', response)


def comment(request, id):
    if request.method == 'GET':
        response = {'commentform': CommentForm, 'status': id}
        return render(request, 'comment_page.html', response)
    elif request.method == 'POST':
        response = {'name': Member.objects.get(id=1).name}
        status = Status.objects.get(id=id)
        form = CommentForm(request.POST)
        response['form'] = form
        print(type(form))
        if form.is_valid():
            Comment.objects.create(name=form.cleaned_data['name'], text=form.cleaned_data['text'], status=status)
            return redirect('/status/')
        else:
            response['form'] = form
            return render(request, 'index.html', response)


def add(request):
    response = {'name': Member.objects.get(id=1).name}
    if request.method == 'POST':
        form = StatusForm(request.POST)
        response['form'] = form
        if form.is_valid():
            Status.objects.create(status=form.cleaned_data['status'])
            status = Status.objects.all()
            response['status'] = status
            return redirect('/status/')
        else:
            status = Status.objects.all()
            response['status'] = status
            response['form'] = form
            return render(request, 'index.html', response)
    return redirect('/status/')


def delete_comment(request, id):
    to_delete = Comment.objects.get(id=id)
    to_delete.delete()
    return redirect('/status/')


def delete_status(request, id):
    to_delete = Status.objects.get(id=id)
    to_delete.delete()
    return redirect('/status/')