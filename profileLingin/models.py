from django.db import models


# Create your models here.

class Member(models.Model):
    name = models.CharField(max_length=27)
    birthday = models.CharField(max_length=27)
    gender = models.CharField(max_length=10, default='Male')
    expertise = models.TextField()
    description = models.TextField()
    email = models.EmailField()
