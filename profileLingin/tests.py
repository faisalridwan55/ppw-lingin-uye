from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import showProfile, submit
from .models import Member
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class ProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_editProfile_url_is_exist(self):
        new_account = Member.objects.create(name='joko', birthday='01 Jan', gender='Male', expertise='Marketing, Collector, Public Speaking', description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com', id="1")
        response = Client().get('/profile/edit/')
        self.assertEqual(response.status_code, 200)

    def test_editProfile_cant_error(self):
        response = Client().get('/profile/edit/')
        self.assertEqual(response.status_code, 302)

    def test_using_showProfile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, showProfile)

    def test_model_can_create_account(self):
        # Creating account
        new_account = Member.objects.create(name='joko', birthday='01 Jan', gender='Male', expertise='Marketing, Collector, Public Speaking', description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com', id="1")
        # Retrieving all available activity
        counting_all_account = Member.objects.all().count()
        self.assertEqual(counting_all_account, 1)

    def test_model_can_store_data(self):
        # Creating account
        new_account = Member.objects.create(name='joko', birthday='01 Jan', gender='Male', expertise='Marketing, Collector, Public Speaking', description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com', id="1")
        account_name = new_account.name
        self.assertEqual(account_name, 'joko')

    def test_showProfile_can_show_data(self):
        response = self.client.get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Zafat', html_response)
    
    def test_submit_function_not_error(self):
        respons = self.client.get('/profile/submit')
        self.assertEqual(respons['location'], '/profile/')

    def test_submit_function(self):
        new_account = Member.objects.create(name='joko', birthday='01 Jan', gender='Male', expertise='Marketing, Collector, Public Speaking', description='Antique expert. Experience as marketer for 10 years', email='hello@joko.com')
        respons = Client().post('/profile/submit', {'id' : 1, 'name': 'bedu', 'birthday': '5 Mei', 'gender': 'Male', 'email': 'fa@yahoo.com', 'expertise': 'makan', 'description': 'hehe'})
        self.assertEqual(respons.status_code, 302)
        self.assertEqual(new_account.id, 1)
