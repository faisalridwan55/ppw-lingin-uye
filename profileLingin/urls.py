from django.conf.urls import url
from .views import showProfile, showEdit, submit
#url for app
urlpatterns = [
    url(r'^$', showProfile, name='showProfile'),
    url(r'^edit', showEdit, name='edit'),
    url(r'^submit', submit, name='submit'),
]