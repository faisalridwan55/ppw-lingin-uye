from django.shortcuts import render
from .models import Member
from django.http import HttpResponseRedirect

# Create your views here.


respons = {}
def showProfile(request):
	try:
		default_user = Member.objects.get(id="1")
	except Exception as e:
		default_user = Member.objects.create(name="Zafat Kasatfa", birthday="7 Juli", gender="Male", expertise="Programming, Analys, Manager, Futsal, Bola, Lari, Atletik, Taekwondo, Renang" ,description="Kita adalah 4 orang pejuang kece", email="zafat.kasatfa@ui.ac.id")
		default_user.save()
	finally:
		default_user = Member.objects.get(id="1")
		expertise = default_user.expertise
		expertise_list = [x.strip() for x in expertise.split(',')]
		expertise_len = len(expertise_list)//3
		expertise_list1 = []
		expertise_list2 = []
		for i in range(expertise_len):
			expertise_list1.append(expertise_list.pop())
		for i in range(expertise_len):
			expertise_list2.append(expertise_list.pop())	
		respons["default_user"] = default_user
		respons["expertise"] = expertise_list
		respons["expertise1"] = expertise_list1
		respons["expertise2"] = expertise_list2
		return render(request, 'profile.html', respons)

def showEdit(request):
	try:
		default_user = Member.objects.get(id="1")
		respons["default_user"] = default_user
		return render(request, 'edit.html', respons)
	except Exception as e:
		return HttpResponseRedirect('/profile/')

def submit(request):
	if(request.method == 'POST'):
		edited_user = Member.objects.get(id=request.POST['id'])
		edited_user.name = request.POST['name']
		edited_user.birthday = request.POST['birthday']
		edited_user.gender = request.POST['gender']
		edited_user.email = request.POST['email']
		edited_user.expertise = request.POST['expertise']
		edited_user.description = request.POST['description']
		edited_user.save()
	
	return HttpResponseRedirect('/profile/')