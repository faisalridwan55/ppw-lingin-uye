"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from status.views import index
from django.views.generic.base import RedirectView
from status.admin import admin_site
import status.urls as status
import profileLingin.urls as profile
import friendList.urls as friend
import stats.urls as stats

urlpatterns = [
    url(r'^admin/', admin_site.urls),
    url(r'^$', RedirectView.as_view(url='status/', permanent=True), name='root'),
    url(r'^status/', include(status, namespace='status')),
    url(r'^profile/', include(profile, namespace='profile')),
    url(r'^friend/', include(friend, namespace='friend')),
    url(r'^stats/', include(stats, namespace='stats')),
]
