from django.apps import AppConfig


class FriendlistConfig(AppConfig):
    name = 'friendList'
