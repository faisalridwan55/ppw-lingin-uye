from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import friend_index, add_friend
from .models import Friend

# Create your tests here.
class profileTest(TestCase):
    def test_friendList_url_is_exist(self):
        response = Client().get('/friend/')
        self.assertEqual(response.status_code, 200)

    def test_friendList_using_index_func(self):
        found = resolve('/friend/')
        self.assertEqual(found.func, friend_index)  

    def test_model_can_create_new_message(self):   
        new_activity = Friend.objects.create(name="agus",url="google.com")
        counting_all_available_message= Friend.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

    def test_friendList_showing_all_messages(self):

        name_agus = 'Agus'
        url_agus = 'google.herokuapp.com'
        data_agus = {'name': name_agus, 'url': url_agus}
        post_data_agus = Client().post('/friend/add_friend', data_agus)
        self.assertEqual(post_data_agus.status_code, 200)

        response = Client().get('/friend/')
        html_response = response.content.decode('utf8')

        for key,data in data_agus.items():
            self.assertIn(data,html_response)

        self.assertIn('Agus', html_response)
        self.assertIn(url_agus, html_response)

    def test_friendList_delete(self):
        todo = Friend.objects.create(name='test', url='123.herokuapp.com')
        response_post = Client().get('/friend/delete/{}/'.format(todo.id))
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(Friend.objects.count(), 0)

    def test_model_can_reject_invalid_message(self):
        name_agus = 'Agus'
        url_agus = 'google.com'
        data_agus = {'name': name_agus, 'url': url_agus}
        post_data_agus = Client().post('/friend/add_friend', data_agus)
        self.assertEqual(post_data_agus.status_code, 200)

        response = Client().get('/friend/')
        html_response = response.content.decode('utf8')
        self.assertIn("alert alert-danger", html_response)


