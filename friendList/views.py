from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

# Create your views here.
def friend_index(request):
    response = {}
    html = 'friendList.html'
    response['Friend_Form'] = Friend_Form
    friend = Friend.objects.all()   
    response['friend'] = friend
    return render(request, html , response)

def add_friend(request):
    response = {}
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friend(name=form.cleaned_data['name'], url=form.cleaned_data['url'])
        friend.save()
    else:        
        response['form'] = form
    html ='friendList.html'
    friend = Friend.objects.all()
    response['friend'] = friend
    return render(request, html, response)

def delete_friend(request, pk):
   todo = Friend.objects.filter(pk=pk).first()
   todo.delete()
   return HttpResponseRedirect('/friend/')
