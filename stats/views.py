from django.shortcuts import render
from profileLingin.models import Member
from friendList.models import Friend
from status.models import Status


# Create your views here.
def index(request):
    try:
        default_user = Member.objects.get(id="1")
    except Exception as e:
        default_user = Member.objects.create(name="Zafat Kasatfa", birthday="7 Juli", gender="Male", expertise="Programming, Analys, Manager, Futsal, Bola, Lari, Atletik, Taekwondo, Renang" ,description="Kita adalah 4 orang pejuang kece", email="zafat.kasatfa@ui.ac.id")
        default_user.save()
    finally:
        response = {}
        default_user = Member.objects.get(id="1")
        number_of_posts = len(Status.objects.all())
        latest_status = Status.objects.get(id=str(number_of_posts))
        profile_picture = "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.theimaginativeconservative.org%2Fwp-content%2Fuploads%2F2015%2F04%2Frobert-downey-jr.png&f=1"

        response['default_user'] = default_user
        response['number_of_posts'] = number_of_posts
        response['number_of_friends'] = len(Friend.objects.all())
        response['profile_picture'] = profile_picture
        response['latest_status'] = latest_status
        return render(request, 'stats.html', response)
