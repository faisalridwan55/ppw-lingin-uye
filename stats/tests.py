from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from status.models import Status
from friendList.models import Friend
from profileLingin.models import Member
# Create your tests here.


class StatsTest(TestCase):
    def test_number_of_post(self):
        for i in range(10):
            Status.objects.create(status="I'm currently happy")
        response = self.client.get('/stats/')
        self.assertIn("Feeds 10 posts", response.content.decode('utf8'))

    def test_number_of_friends(self):

        Status.objects.create(status="I'm currently happy")

        Member.objects.create(name='joko', birthday='01 Jan',
                gender='Male',
                expertise='Marketing, Collector, Public Speaking',
                description='Antique expert. Experience as marketer for 10 years',
                email='hello@joko.com',
                id="1")
        names = ['kucing', 'terbang', 'ingin']
        urls = ['link-in.herokuapp.com',
                'cool-ppw-app.herokuapp.com',
                'my-first-TDD.herokuapp.com']
        post_data = None
        for i in range(len(names)):
            data = {'name': names[i], 'url': urls[i]}
            post_data = self.client.post('/friend/add_friend', data=data)
        response = self.client.get('/stats/')
        html_response = response.content.decode('utf8')

        self.assertIn('Friends 3 people', html_response)

    def test_latest_post(self):
        Member.objects.create(name='joko', birthday='01 Jan',
                gender='Male',
                expertise='Marketing, Collector, Public Speaking',
                description='Antique expert. Experience as marketer for 10 years',
                email='hello@joko.com',
                id="1")
        test_message = "i want to eat noodle";
        response = self.client.post('/status/add/', data={'status':test_message})
        new_response = self.client.get('/status/')
        html_response = new_response.content.decode('utf8')
        self.assertIn(test_message, html_response)
        pass
